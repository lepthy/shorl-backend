### Disclaimer

This project was done in a limited timeframe, there is a LOT of refactoring to do.  
This project simplify some operation for the same reason (no database encryption for password, authentification only on write, etc)  
  
This is a rest API supposed to run inside a DOCKER container.  
  
This project is inherently scalable, start as many docker as necessary to handle the charge.  
The database layer (mongodb) is scalable, run it as a cluster behind a load balancer for that [documentation](https://docs.mongodb.org/v3.0/core/sharded-cluster-architectures-production/).

### Prerequisites

Run these commands to startup the database layer of the application.  
  
```bash
sudo apt-get install docker.io  
sudo docker pull mongo  
sudo docker run -d -p 27017:27017 -v /mnt/mongodb:/data/db --restart=always mongo --smallfiles  
```

```html
<span style="color: red; font-size: 22px;"> /!\ Edit shorl-backend/sources/shorl.ini and add the mongo host IP address</span>  
```
  
Run these commands to create and run the shorl docker image  
  
```bash
sudo docker build -t shorl shorl-backend  
sudo docker run -i -t -p 1337:1337 shorl  
```

### Test

Once the containers (mongo, shorl) are running:

```bash
$> cd test
$> python3 test.py
.....
----------------------------------------------------------------------
Ran 5 tests in 0.024s

OK
```

### Authentification

Basic Auth for /shorten  
Default user/password: guest/guest  

### Use

```bash
$> echo {\"user\": \"lepthy\", \"origin\": \"http://www.google.com\"} > payload.txt  
$> curl --user guest:guest -X PUT -T payload.txt localhost:1337/shorten  
200 {"short": "q2Vlxud36N"}  
  
$> curl localhost:1337/retrieve/q2Vlxud36N  
200 {"origin": "http://www.google.com"}  

$> curl localhost:1337/retrieve/q2  
404  
  
$> echo {\"user\": \"lepthy\", \"origin\": \"http://www.google.com\", \"short\": \"q2Vlxud36N\"} > payload.txt  
$> curl --user guest:guest -X PUT -T payload.txt localhost:1337/shorten  
409  
  
$> echo {\"user\": \"lepthy\", \"origin\": \"http://www.google.com\", \"short\": \"abba\"} > payload.txt  
$> curl --user guest:guest -X PUT -T payload.txt localhost:1337/shorten  
200 {"short": "abba"}  
  
$> curl localhost:1337/retrieve/abba  
200 {"origin": "http://www.google.com"}  
  
$> curl localhost:1337/info/abba  
200 {  
    "accessed": {  
        "$date": 1454592704153  
    },  
    "created": {  
        "$date": 1454591866017  
    },  
    "origin": "http://www.google.com",  
    "retrieved": 1,  
    "short": "abba",  
    "user": "lepthy"  
}  
  
$>  curl localhost:1337/user/lepthy  
200 [  
    {  
        "accessed": {  
            "$date": 1454591759585  
        },  
        "created": {  
            "$date": 1454591759585  
        },  
        "origin": "http://www.google.com",  
        "retrieved": 1,  
        "short": "abba",  
        "user": "lepthy"  
    },  
    {  
        "accessed": {  
            "$date": 1454591785770  
        },  
        "created": {  
            "$date": 1454591785770  
        },  
        "origin": "http://www.google.com",  
        "retrieved": 1,  
        "short": "q2Vlxud36N",  
        "user": "lepthy"  
    },
	...  
]  
```
