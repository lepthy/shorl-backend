FROM	ubuntu:14.04

RUN		apt-get update 
RUN		apt-get -y upgrade

RUN		apt-get -y install python3 python3-pip python3-dev

RUN		pip3 install --upgrade pip

ENV WORKDIR=sources
ENV XUSER=shorl

ADD $WORKDIR /$WORKDIR
WORKDIR /$WORKDIR

RUN pip3 install -r requirements.txt

RUN useradd -s /bin/bash $XUSER
RUN chown $XUSER:nogroup /$WORKDIR -R

EXPOSE 1337

USER $XUSER
CMD bash entrypoint.sh
