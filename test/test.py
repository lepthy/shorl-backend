import unittest
import json
import base64
import string
import random

from http.client import HTTPConnection

class TestShorlApi(unittest.TestCase):
	def test_0_auth(self):
		auth = base64.b64encode(b"doesnot:exist").decode()

		header = {'Authorization' : 'Basic {}'.format(auth)}
		body = json.dumps({"user": "fail", "origin": "http://www.google.com"})

		conn = HTTPConnection('127.0.0.1:1337')
		conn.request('PUT', '/shorten', body, header)
		response = conn.getresponse()
		conn.close()

		self.assertEqual(response.status, 403)

	def test_1_shorten(self):
		auth = base64.b64encode(b"guest:guest").decode()

		header = {'Authorization' : 'Basic {}'.format(auth)}
		body = json.dumps({"user": "test", "origin": "http://www.google.com"})

		conn = HTTPConnection('127.0.0.1:1337')
		conn.request('PUT', '/shorten', body, header)
		response = conn.getresponse()
		datastr = response.read().decode()
		conn.close()

		data = json.loads(datastr)

		self.assertEqual(response.status, 200)
		self.assertTrue('short' in data)

		global MNEMO
		MNEMO = data

	def test_2_conflict(self):
		auth = base64.b64encode(b"guest:guest").decode()

		header = {'Authorization' : 'Basic {}'.format(auth)}
		body = json.dumps({"user": "test", "origin": "http://www.google.com", "short": MNEMO["short"]})

		conn = HTTPConnection('127.0.0.1:1337')
		conn.request('PUT', '/shorten', body, header)
		response = conn.getresponse()
		conn.close()

		self.assertEqual(response.status, 409)
		
	def test_3_shorten_defined(self):
		auth = base64.b64encode(b"guest:guest").decode()

		short = str.join('', [random.SystemRandom().choice(string.ascii_lowercase) for _ in range(8)])

		header = {'Authorization' : 'Basic {}'.format(auth)}
		body = json.dumps({"user": "test", "origin": "http://www.google.com", "short": short})

		conn = HTTPConnection('127.0.0.1:1337')
		conn.request('PUT', '/shorten', body, header)
		response = conn.getresponse()
		datastr = response.read().decode()
		conn.close()

		data = json.loads(datastr)

		self.assertEqual(response.status, 200)
		self.assertTrue('short' in data)
		self.assertEqual(data['short'], short)

	def test_4_retrieve(self):
		conn = HTTPConnection('127.0.0.1:1337')
		conn.request('GET', '/retrieve/{}'.format(MNEMO['short']))
		response = conn.getresponse()
		datastr = response.read().decode()
		conn.close()

		data = json.loads(datastr)

		self.assertEqual(response.status, 200)
		self.assertTrue('origin' in data)

if __name__ == '__main__':
	unittest.main()
