#!/usr/bin/python3

import datetime
import random
import string

import traceback
import base64

import pymongo
import json
from bson import json_util

import configparser
import argparse
import logging

import tornado.ioloop
import tornado.web

### SETUP

def mongo_connect():
	return pymongo.MongoClient(
		host = SETTINGS.get('mongo', 'host'),
		port = int(SETTINGS.get('mongo', 'port'))
	)

def get_config(filename):
	settings = configparser.ConfigParser()
	settings.read(filename)
	return settings

def get_logger(level):
	name = SETTINGS.get('log', 'facility')
	logger = logging.getLogger(name)

	if not level:
		level = SETTINGS.get('log', 'default_level')
	logger.setLevel(level.upper())

	#add formatter if needed
	#add handler if needed
	return logger

### API

def require_basic_auth(handler_class):
	def wrap_execute(handler_execute):
		def require_basic_auth(handler, kwargs):
			auth_header = handler.request.headers.get('Authorization')
			if auth_header is None or not auth_header.startswith('Basic '):
				handler.set_status(401)
				handler.set_header('WWW-Authenticate', 'Basic realm=Restricted')
				handler._transforms = []
				handler.finish()
				return False
			auth_decoded = base64.decodestring(auth_header[6:].encode())
			kwargs['basicauth_user'], kwargs['basicauth_pass'] = auth_decoded.decode().split(':', 2)
			kwargs['auth'] = 'True' if MONGO['shorl']['users'].find_one({'user': kwargs['basicauth_user'], 'password': kwargs['basicauth_pass']}) else 'False'
			return True 
		def _execute(self, transforms, *args, **kwargs):
			if not require_basic_auth(self, kwargs):
				return False
			return handler_execute(self, transforms, *args, **kwargs)
		return _execute

	handler_class._execute = wrap_execute(handler_class._execute)
	return handler_class

class Static(tornado.web.RequestHandler):
	def get(self):
		pass

class Retrieve(tornado.web.RequestHandler):
	def abort(self, code):
		self.clear()
		self.set_status(code)
		self.finish()

	def get(self, short):
		entry = MONGO['shorl']['urls'].find_one({'short': short})
		if not entry:
			self.abort(404)
			return
		else:
			MONGO['shorl']['urls'].update_one(
				{'short': short},
				{
					'$set': {
						'accessed': datetime.datetime.utcnow()
					},
					'$inc': {
						'retrieved': 1
					}
				}
			)
		response = {
			'origin': entry['origin']
		}
		self.write(json.dumps(response))

class Info(tornado.web.RequestHandler):
	def get(self, short):
		entry = MONGO['shorl']['urls'].find_one({'short': short}, {'_id': False})
		if not entry:
			self.abort(404)
			return
		else:
			response = json.dumps(entry, indent=4, sort_keys=True, default=json_util.default)
			self.write(response)

class User(tornado.web.RequestHandler):
	def get(self, user):
		entry = MONGO['shorl']['urls'].find({'user': user}, {'_id': False})
		if not entry.count():
			self.abort(404)
			return
		else:
			entry = [element for element in entry]
			response = json.dumps(entry, indent=4, sort_keys=True, default=json_util.default)
			self.write(response)

@require_basic_auth
class Shorten(tornado.web.RequestHandler):
	def abort(self, code):
		self.clear()
		self.set_status(code)
		self.finish()

	def generator(self, max_size=23, chars=string.ascii_lowercase+string.ascii_uppercase+string.digits):
		#could use minimum vaue possible and increase on each try
		#chance to fall several time on existing values is close to nil
		size = random.SystemRandom().randint(1, max_size)
		short = str.join('', [random.SystemRandom().choice(chars) for _ in range(size)])
		return short

	def addshort(self, user, origin, short=None):
		if short and MONGO['shorl']['urls'].find_one({'short': short}):
			self.abort(409)
			return
		elif not short:
			bexists = True
			while bexists: #could add max_try
				short = self.generator()
				bexists = True if MONGO['shorl']['urls'].find_one({'short': short}) else False

		entry = {
			'short': short,
			'origin': origin,
			'user': user,
			'retrieved': 0,
			'created': datetime.datetime.utcnow(),
			'accessed': datetime.datetime.utcnow()
		}

		MONGO['shorl']['urls'].insert_one(entry)

		response = {
			'short': short
		}
		self.write(json.dumps(response))

	def put(self, **kwargs):
		if kwargs['auth'] != 'True':
			self.abort(403)
			return

		try:
			payload = json.loads(self.request.body.decode())
		except Exception:
			print(traceback.format_exc())	
			self.abort(400)
			return

		if not 'short' in payload and 'origin' in payload:
			self.addshort(payload['user'], payload['origin'])
		elif 'short' in payload and 'origin' in payload:
			self.addshort(payload['user'], payload['origin'], payload['short'])

def run():
	application = tornado.web.Application(
		[
			(r'/shorten', Shorten),
			(r'/retrieve/(.*)', Retrieve),
			(r'/info/(.*)', Info),
			(r'/user/(.*)', User),
			(r'/static', Static)
		]
	)

	application.listen(
		int(SETTINGS.get('api', 'port')),
		address=SETTINGS.get('api', 'host')
	)
	tornado.ioloop.IOLoop.instance().start()

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('-c', '--config-file', type=str)
	parser.add_argument('-l', '--log-level', type=str)
	args = parser.parse_args()

	SETTINGS = get_config(args.config_file)
	MONGO = mongo_connect()
	LOGGER = get_logger(args.log_level)

	#add guest user
	MONGO['shorl']['users'].insert_one({'user': 'guest', 'password': 'guest'})

	try:
		run()
	except Exception:
		LOGGER.error('Unexpected Error, traceback follows: {}'.format(traceback.format_exc()))
